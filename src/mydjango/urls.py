"""mydjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings





# Routers provide a way of automatically determingn the URL conf
#Wire up our API using automatic URL routing
#Additionally we include login URLS for the browsable API




urlpatterns = [
    url(r'^item-detail/(?P<pk>\d+)/$', views.ItemDetailView.as_view(), name='item-detail'),
    url(r'^item-list/$', views.ItemListView.as_view(), name='item-list'),
    url(r'^rental-panel-item-list/$', views.rental_panel_item_list, name='rental-panel-item-list'),
    url(r'^rental-panel-registration/$', views.rental_panel_registration, name='rental-panel-registration'),
    url(r'^rental-panel-login/$', views.rental_panel_login, name='rental-panel-login'),
    url(r'^rental-panel-statistics/$', views.rental_panel_statistics, name='rental-panel-statistics'),
    url(r'^rental_panel-settings/$', views.rental_panel_settings, name='rental_panel-settings'),
    url(r'^admin/', admin.site.urls),
<<<<<<< HEAD
    url(r'', include('myapp.urls')),
=======
    url(r'^$', views.MainPageListView.as_view(), name='main-page'),

>>>>>>> a803502bb59436895c2897141ddbecc276dde813
]
# urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
