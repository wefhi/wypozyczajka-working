from django.conf.urls import url, include
from . import views
from . import serializers
from . import routers

app_name = 'myapp'

urlpatterns = [
    url(r'^', include(routers.router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
