from django.views import generic
from django.shortcuts import render
from django.views.generic import ListView, DetailView
from .models import Item

class MainPageListView(ListView):
    model = Item
    template_name = 'main_page.html'

class ItemDetailView(DetailView):
    model = Item
    template_name = 'item_detail.html'

class ItemListView(ListView):
    model = Item
    template_name = 'item_list.html'

def rental_panel_item_list(request):
    return render(request, 'rental_panel_item_list.html')

def rental_panel_registration(request):
    return render(request, 'rental_panel_registration.html')

def rental_panel_login(request):
    return render(request, 'rental_panel_login.html')

def rental_panel_statistics(request):
    return render(request, 'rental_panel_statistics.html')

def rental_panel_settings(request):
    return render(request, 'rental_panel_settings.html')
