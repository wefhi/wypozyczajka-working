# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-12-01 17:44
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Photograph',
        ),
    ]
