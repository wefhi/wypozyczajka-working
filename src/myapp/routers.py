from rest_framework import routers
from myapp.serializers import ChestViewSet
from myapp.serializers import ItemViewSet

# Routers provide a way of automatically determingn the URL conf
router = routers.DefaultRouter()

router.register(r'chests', ChestViewSet)
router.register(r'items', ItemViewSet)
